// ref:
// - https://umijs.org/plugins/api
import { IApi } from '@umijs/types';
import { existsSync, mkdirSync, writeFileSync } from 'fs';
import { join } from 'path';

export default (api: IApi) => {
  const joinAbsPath = (path: string) =>
    join(api.paths.absTmpPath!, 'firebase', path);

  api.describe({
    key: 'firebase',
    config: {
      schema(joi) {
        return joi.object({
          apiKey: joi.string(),
          authDomain: joi
            .string()
            .domain()
            .optional(),
          databaseURL: joi
            .string()
            .uri()
            .optional(),
          projectId: joi.string(),
          storageBucket: joi
            .string()
            .domain()
            .optional(),
          messagingSenderId: joi.string().optional(),
          appId: joi.string(),
          measurementId: joi.string().optional(),
          features: joi
            .array()
            .items(
              'analytics',
              'auth',
              'firestore',
              'functions',
              'messaging',
              'storage',
              'performance',
              'database',
              'remote-config',
            )
            .default([]),
        });
      },
    },
  });

  api.onGenerateFiles(() => {
    const firebaseAbsPath = joinAbsPath('');
    if (!existsSync(firebaseAbsPath)) {
      mkdirSync(firebaseAbsPath);
    }

    const { features, ...config } = api.userConfig.firebase;

    if (process.env.NODE_ENV === 'production' && !config.apiKey) {
      api.logger.error(
        `In production 'firebase apiKey option' cannot be null.`,
      );
    }

    const indexPath = joinAbsPath('index.tsx');

    const code =
      "import firebase from 'firebase/app';\n" +
      config
        .map((feature: string) => `import \'firebase/${feature}\';\n`)
        .join('') +
      '\nconst config = "<%= Config %>";\n' +
      '\n' +
      'firebase.initializeApp(config);\n' +
      (config.contains('analytics') ? 'firebase.analytics();\n' : '') +
      (config.contains('performance') ? 'firebase.performance();\n' : '');

    const indexContent = !config.apiKey
      ? ''
      : code.replace('"<%= Config %>"', JSON.stringify(config));

    writeFileSync(indexPath, indexContent);
  });

  api.addEntryImports(() => {
    return [
      {
        source: './firebase/index',
      },
      {
        source: 'firebase/app',
        specifier: 'firebase',
      },
    ];
  });
};
